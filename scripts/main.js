// 2 вариант реализовано через  Promise.all, с 1 fetch-запросом

Promise.all([
  fetch(`https://ajax.test-danit.com/api/json/users`).then(res => res.json()),
  fetch(`https://ajax.test-danit.com/api/json/posts`).then(res => res.json()),
]).then(arr => {
  console.log(arr)

  const [users, posts] = arr;

  users.forEach(({id: number, name, username, email}) => {
    posts.forEach(({id, body, title, userId}) => {
      if (number === userId) {
        new Posts(id, name, username, email, title, body, EditForm).render()
      }
    })
  })
}).catch(err => console.log(err)) 



// 2 вариант - рабочий но тут 10 запросов  так как второй fetch запращивается при пеерборе(forEach) масива результатова data из  первого fetch

// fetch(`https://ajax.test-danit.com/api/json/users`)
//   .then(res => {
//     if (res.status === 200) {
//       return res.json();
//     }
//   })
//   .then(data => {
    
//     console.log(data)
//     data.forEach(({id: number, name, username, email}) => {

//         fetch(`https://ajax.test-danit.com/api/json/posts`)
//           .then(res => {
//             return res.json();
//           })
//           .then(data => {
//             data.forEach(({id, body, title, userId}) => {
//               if (number === userId) {
//                 new Posts(id, name, username, email, title, body, EditForm).render()
//               }
//             })
//           })
//           .catch(err => console.log(err)) 
//     });
// })
// .catch(err => console.log(err)) 

  class EditForm {
    constructor(title, text, onSubmit) {
      this.title = title;
      this.text = text;
      this.onSubmit = onSubmit;
      this.container = document.querySelector('.container');
      this.form = document.createElement('form');
      this.titleInput = document.createElement('input');
      this.textInput = document.createElement('textarea');
      this.submitButton = document.createElement('button');
      this.cancelButton = document.createElement('button');
    }
  
    createElements() {
      this.form.className = 'form';
      this.titleInput.className = 'form__input';
      this.textInput.className = 'form__textarea';
      this.submitButton.className = 'btn';
      this.submitButton.innerHTML = `Submit`
      this.cancelButton.className = 'btn';
      this.cancelButton.innerHTML = 'Cancel';
      this.titleInput.value = this.title;
      this.textInput.value = this.text;
      this.form.append(this.titleInput, this.textInput, this.submitButton, this.cancelButton);

      this.cancelButton.addEventListener('click', (e) => {
        this.remove() 
      })
  
      this.form.addEventListener('submit', (e) => {
        e.preventDefault();
        this.onSubmit(this.titleInput.value, this.textInput.value);
      });
    }
  
    remove() {
      this.form.remove();
    }
  
    render() {
      this.createElements();
      this.container.append(this.form);
    }
  }

class Posts {
  constructor(id, name, username, email, title, body, EditForm) {
    this.id = id;
    this.name = name;
    this.username = username;
    this.email = email;
    this.title = title;
    this.body = body;
    this.EditForm = EditForm;
    this.container = document.querySelector('.container');
    this.card = document.createElement('div');
    this.cardBoxInfo = document.createElement('div');
    this.titleElem = document.createElement('h2');
    this.textElem = document.createElement('p');
    this.boxBtn = document.createElement('div'); 
    this.deleteButton = document.createElement('button');
    this.correctButton = document.createElement('button');
    this.loader = document.querySelector('.loader')
  }

  createElements() {
      this.loader.style.display = 'none';

      this.card.className = 'card';
      this.card.innerHTML = `
          <div class="card__box">
            <p class="card-info__name">${this.name}</p>
            <p class="card-info__secondname">${this.username}</p>
            <p class="card-info__email">${this.email}</p>
          </div>

      `
      this.cardBoxInfo.className = 'card__box-info';
      this.titleElem.className = 'card__caption';
      this.textElem.className = 'card__text';
      this.titleElem.innerText = this.title;
      this.textElem.innerText = this.body;

      this.boxBtn.className = 'card__box-btn';
      this.deleteButton.className = 'btn';
      this.deleteButton.innerHTML = `Delete post`

      this.correctButton.className = 'btn btn-correct';
      this.correctButton.innerHTML = `Correct post`
      
      this.cardBoxInfo.append(this.titleElem, this.textElem)
      this.boxBtn.append(this.deleteButton)
      this.boxBtn.append(this.deleteButton, this.correctButton )
      this.card.append(this.cardBoxInfo, this.boxBtn)


    this.correctButton.addEventListener('click', e => {

      const form = new this.EditForm(this.title, this.body, (updatedTitle, updatedText) => {

        const body = {
          title: updatedTitle,
          body: updatedText,
        };

        fetch(`https://ajax.test-danit.com/api/json/posts/${this.id}`, {
          method: 'PUT',
          body: JSON.stringify(body),
          headers: {
            'Content-Type': 'application/json'
          }
        })
          .then(response => response.json())
          .then(data => {

            console.log(data)
            const {title, body} = data;
            // this.title = title;
            // this.body = body;

            this.titleElem.innerText = title;
            this.textElem.innerText = body;

            form.remove();

          })
          .catch(err => console.log(err))

      });
      form.render();
    })

      this.deleteButton.addEventListener('click', e => {
        fetch(`https://ajax.test-danit.com/api/json/posts/${this.id}`, {
          method: 'DELETE',
        }) 
        .then(response  => {
          if(response.status === 200) {
            this.card.remove();
          }
        })
        .catch(err => console.log(err)) 
      }) 
  } 

  render() {
    this.createElements();
    this.container.append(this.card)
  }
}

class NewPost extends Posts {
  constructor(title, body) {
    super(title, body)
    this.title = title;
    this.body = body;
    this.id = 1;
    this.name = 'Leanne Graham';
    this.username = 'Bret';
    this.email = 'Sincere@april.biz';
  }

  // createElements() {
  //   super.createElements()
  // }

  render() {
    super.render()
    this.container.prepend(this.card)
  }
}

const btnAdd = document.querySelector('.btn-add');
const modal = document.querySelector('.modal');
const modalForm = document.querySelector('.modal-form');
const submitAdd = document.querySelector('.submit-add');
const submitChanged = document.querySelector('.submit-changed');
const buttonCancel = document.querySelector('.button-cancel');

btnAdd.addEventListener('click', (e) => {
  modal.style.display = 'block';
});

submitAdd.addEventListener('click', (e) => {
  e.preventDefault();

  const loader = document.querySelector('.loader');
  loader.style.display = 'block';

  const body = {};
  
  modalForm.querySelectorAll('input').forEach(input => {
    body[input.name] = input.value;
  })
  console.log(body)

  fetch(`https://ajax.test-danit.com/api/json/posts`, {
    method: 'POST',
    body: JSON.stringify(body),
    headers: {
      'Content-Type': 'application/json'
    }
  })
  .then(response => {
    if (response.status === 200) {
      return response.json();
    }
  })
  .then(({title, text}) => {
    new NewPost(title, text).render()
    modal.style.display = 'none';
    document.querySelector('input').value = '';
    document.querySelector('.text-publication').value = '';
  })
  .catch(err => console.log(err))

})

buttonCancel.addEventListener('click', (e) => {
  modal.style.display = 'none';
})


